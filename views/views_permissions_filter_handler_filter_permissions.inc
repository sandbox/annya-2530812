<?php

/**
 * @file
 * Definition of views_permissions_filter_handler_filter_permissions.
 */

/**
 * A handler to check user permissions.
 */

class views_permissions_filter_handler_filter_permissions extends views_handler_filter {

  function can_expose() {
    return FALSE;
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['perm'] = array('default' => 'access content');

    return $options;
  }

  /**
   * Overrides views_handler_filter#options_form().
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $perms = array();
    $module_info = system_get_info('module');

    // Get list of permissions
    foreach (module_implements('permission') as $module) {
      $permissions = module_invoke($module, 'permission');
      foreach ($permissions as $name => $perm) {
        $perms[$module_info[$module]['name']][$name] = strip_tags($perm['title']);
      }
    }

    ksort($perms);

    $form['perm'] = array(
      '#type' => 'select',
      '#options' => $perms,
      '#title' => t('Permission'),
      '#default_value' => $this->options['perm'],
      '#description' => t('Only users with the selected permission flag will be able to access this content.'),
    );
  }

  /**
   * Overrides views_handler_filter#query().
   */
  function query() {
    if (user_access($this->options['perm'])) {
      $this->query->add_where_expression($this->options['group'], 1);
    }
  }

  /**
   * Overrides views_handler_filter#admin_summary().
   */
  function admin_summary() {
    $permissions = module_invoke_all('permission');
    if (isset($permissions[$this->options['perm']])) {
      return $permissions[$this->options['perm']]['title'];
    }

    return t($this->options['perm']);
  }
}
