<?php
/**
 * Implements hook_views_data_alter().
 */
function views_permissions_filter_views_data_alter(&$data) {
  // Global filter for user permissions.
  $data['views']['permissions'] = array(
    'title' => t("Check user's permissions"),
    'help' => t("Check user's permissions."),
    'filter' => array(
      'handler' => 'views_permissions_filter_handler_filter_permissions',
    ),
  );
}
